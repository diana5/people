package com.people.people.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.util.MultiValueMap;

import com.people.people.model.Persona;

public interface PersonaRepository extends JpaRepository <Persona,Integer> {

	Optional<Persona> findById(Integer id);

}
