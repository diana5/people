package com.people.people.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.springframework.web.bind.annotation.RestController;
import javax.persistence.Id;


@Entity(name="persona")
public class Persona {
	
	@Column(name="id")
	@Id
	public Integer id;
	
	@Column(name="nombre")
	public String nombre;
	
	public Integer getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getEdad() {
		return edad;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	@Column(name="apellido")
	public  String apellido;
	
	@Column(name="edad")
	public String edad;
	

}
