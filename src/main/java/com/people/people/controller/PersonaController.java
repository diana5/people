package com.people.people.controller;


import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.people.people.model.Persona;
import com.people.people.repository.PersonaRepository;



@RestController
@RequestMapping(value = "/V1/perso")
public class PersonaController {

	@Autowired
	PersonaRepository personaRepository;

	// Retorna todas la lista de persona
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Object> listarpersonas() {

		return new ResponseEntity<Object>(personaRepository.findAll(), HttpStatus.OK);
		
		

		
	}

	// retorna una persona
	@RequestMapping(method = RequestMethod.GET, path = "/{id}")
	public ResponseEntity<Object> listarunapersona(@PathVariable(value="id") Integer id) {

		return new ResponseEntity<Object>(personaRepository.findOne(id), HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> crearpersona(@Valid @RequestBody Persona persona) {
		personaRepository.save(persona);
		return new ResponseEntity<Object>(persona, HttpStatus.CREATED);
	}

	// editar
	@RequestMapping(method = RequestMethod.PUT, path = "/{id}")
	public ResponseEntity<Persona> actualizarpersona(@PathVariable(value = "id") Integer personaid,
			@Valid @RequestBody Persona detallepersona) {

		Optional<Persona> persona = personaRepository.findById(personaid);
		if (!persona.isPresent())
			return new ResponseEntity<Persona>(new Persona(), HttpStatus.NO_CONTENT);

		persona.get().setApellido(detallepersona.getApellido());

		persona.get().setId(detallepersona.getId());

		persona.get().setNombre(detallepersona.getNombre());

		persona.get().setEdad(detallepersona.getEdad());
		personaRepository.save(persona.get());

		return new ResponseEntity<Persona>(persona.get(), HttpStatus.OK);
	}
	
	//Eliminar
	@RequestMapping(method= RequestMethod.DELETE, path= "/{id}")
	public ResponseEntity<Void> eliminarpersona(@PathVariable("id") Integer id){
		personaRepository.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);

	}

}
